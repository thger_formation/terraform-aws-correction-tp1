#!/bin/bash

echo "${my_public_key}" >> /home/${main_user}/.ssh/authorized_keys

apt update
apt -y upgrade
apt -y install mysql-client curl netcat apache2 ghostscript libapache2-mod-php php php-bcmath php-curl php-imagick php-intl php-json php-mbstring php-mysql php-xml php-zip

echo "Endpoint RDS : ${rds_endpoint}" >> /home/${main_user}/.cred_rds
echo "User RDS : ${rds_user}" >> /home/${main_user}/.cred_rds
echo "Password RDS : ${rds_password}" >> /home/${main_user}/.cred_rds
