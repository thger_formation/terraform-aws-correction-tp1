# N'oubliez pas d'export vos AK/SK ou faite un aws configure
# export AWS_ACCESS_KEY_ID=
# export AWS_SECRET_ACCESS_KEY=

tf_fmt:
	terraform validate .
	terraform fmt

tf_apply:
	terraform fmt
	terraform validate .
	terraform apply --var-file=demo.tfvars

tf_destroy:
	terraform destroy --var-file=demo.tfvars
