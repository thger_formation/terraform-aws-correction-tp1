resource "aws_kms_key" "main" {
  description             = "KMS key for ${var.prefix}"
  deletion_window_in_days = 10
  tags                    = var.default_tags
}
