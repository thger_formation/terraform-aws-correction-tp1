data "aws_vpc" "default" {
  default = true
}

#On récupère les subnetes par defaut
data "aws_subnet" "region" {
  count = length(var.subnetes_region)
  id    = var.subnetes_region[count.index]
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

data "template_file" "instance" {
  template = file("user_data.tpl")
  vars = {
    my_public_key = var.my_public_key
    main_user     = "ubuntu"
    rds_user      = var.rds_user
    rds_password  = random_password.password.result
    rds_endpoint  = aws_db_instance.this.endpoint
  }
}

#Petite astuce pour avoir son IPv4 en tant qu'objet
data "http" "myip" {
  url = "http://ipv4.icanhazip.com"
}
